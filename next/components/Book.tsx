"use client";

import { useQuery } from "@apollo/experimental-nextjs-app-support/ssr";
import gql from "graphql-tag";

type Props = {
  slug: string;
};

export default function Book({ slug }: Props) {
  const { data } = useQuery(
    gql`
      query Book($slug: String!) {
        book(slug: $slug) {
          author
          slug
          title
        }
      }
    `,
    { variables: { slug } }
  );

  if (data && data.book) {
    return (
      <>
        <h1>{data.book.title}</h1>
        <h2>{data.book.author}</h2>
      </>
    );
  }

  return <p>Loading...</p>;
}
