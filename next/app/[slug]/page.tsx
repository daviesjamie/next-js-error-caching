import gql from "graphql-tag";
import { notFound } from "next/navigation";
import Book from "../../components/Book";
import { getClient } from "../../lib/ApolloClient";

type Props = {
  params: { slug: string };
};

export async function generateMetadata({ params }: Props) {
  const { data } = await getClient().query({
    query: gql`
      query Book($slug: String!) {
        book(slug: $slug) {
          author
          slug
          title
        }
      }
    `,
    variables: { slug: params.slug },
  });

  if (!data || !data.book) {
    return notFound();
  }

  return {
    title: `${data.book.title} - ${data.book.author}`,
  };
}

export default function BookPage({ params }: Props) {
  return <Book slug={params.slug} />;
}
